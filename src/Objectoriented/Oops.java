package Objectoriented;

interface ItemDetails
{
	void ItmDetails();
}

class Items implements ItemDetails
{	
	
	String ItemName;
	String BrandName;
	int Price;
	
	public Items(String itemName, String brandName, int price) {
		super();
		ItemName = itemName;
		BrandName = brandName;
		Price = price;
	}

	public void ItmDetails()
	{
		System.out.println("The Price Of "+ItemName+" from "+BrandName+" is "+Price);
	}
	
	public void message()
	{
		System.out.println("Everything Is Alright");
	}
}

class Seller extends Items
{

	public Seller(String itemName, String brandName, int price) {
		super(itemName, brandName, price);
	}
		public void message()
		{
			System.out.println("Everything Is not Roght");
		}
		
		public void SellerDtls(int idno)
		{
			System.out.println("The Id Number of The Seller is "+idno);
		}
		
		public void SellerDtls(String name,String place)
		{
			System.out.println("The Name Of The Seller is "+name+" and he is from "+place);
		}
		
		public void SellerDtls(int age,int phoneno)
		{
			System.out.println("he is "+age+" years old. And you can call him @"+phoneno);
		}
		
	}
	


public class Oops {

	public static void main(String[] args) {
		
		
		Items itms = new Items("Tooth Paste","Colgate",25);
		itms.ItmDetails();
		Items itm = new Seller("Tooth Paste","Colgate",25);
		itm.message();
		Seller dtls = new Seller("Tooth Paste","Colgate",25);
				
		dtls.SellerDtls(25);
		dtls.SellerDtls("Nabeel","kayamkulam");
		dtls.SellerDtls(25,2448046);
	}

}
